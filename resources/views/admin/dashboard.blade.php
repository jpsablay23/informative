<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Auto GC</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.2/css/buttons.dataTables.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased">
        <div class="body-container">
            <div class="container">
                <div class="content">
                    <button onclick="document.getElementsByClassName('sidebar')[0].classList.toggle('collapsed')">
                        <div></div>
                        <div></div>
                        <div></div>
                    </button>
                </div>
                @include('admin.adminNav') 
            </div>
            <div class="content-form">
                <button type="button" class="createCategory">Create Category</button>
                <button type="button" class="updateCategory">Update Category</button>
                <button type="button" class="deleteCategory">Delete Category</button>
                <table id="products" class="table table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Product Category</th>
                            <th>Product name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Product Description</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Product Category</th>
                            <th>Product name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Product Description</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="modal fade" id="success" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Successfully added!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="success-delete" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Successfully deleted!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="updateModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Successfully Updated!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="deleteModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <h4 class="delete-desc">Are you sure you want to delete?</h4>
                            <button type="button" class="delete" >Yes</button>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">no</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal bd-example-modal-lg" id="updateFormModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form">
                        <form id="updateProduct" method="post" autocomplete="off">
                            <input type="hidden" name="id" id="id">
                            <div class="row-form">
                                <label>Product category</label>
                                <!-- <input type="text" name="category" id="category" placeholder="Product Category"> -->
                                <select  id="category" name="category" class="form-control">
                                    <option value="">-- Select Category --</option>
                                    @foreach ($category as $data)
                                    <option value="{{$data->cat_id}}">
                                        {{$data->category_name}}
                                    </option>
                                    @endforeach
                                </select>
                                <small class="error_category"></small>
                            </div>
                            <div class="row-form">
                                <label>Product name</label>
                                <input type="text" name="product_name" id="product_name" placeholder="Product Name">
                                <small class="error_product_name"></small>
                            </div>
                            <div class="row-form">
                                <label>Price</label>
                                <input type="number" name="price" id="price" placeholder="Price">
                                <small class="error_price"></small>
                            </div>
                            <div class="row-form">
                                <label>Quantity</label>
                                <input type="number" name="qty" id="qty" placeholder="quantity">
                                <small class="error_qty"></small>
                            </div>
                            <div class="row-form">
                                <label for="message">Product Description</label>
                                <div id="editor"></div>
                                <textarea name="product_description" id="product_description" placeholder="Product description"></textarea>
                                <small class="error_product_description"></small>
                            </div>
                            <div class="row-form">
                                <div class="check_image hide">
                                    <img src="" class="avatar" width="150" height="150"/>
                                    <button type="button" class="change_image hide">Change Image</button>
                                </div>
                                <label class="has_image">Image</label>
                                <input class="has_image" type="file" name="image" id="image">
                                <small class="error_product_image has_image"></small>
                            </div>
                            <button class="updateForm">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
                </div>
            </div>
        </div>
        <div class="modal bd-example-modal-lg" id="categoryFormModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form">
                        <form id="categoryProduct" method="post" autocomplete="off">
                            <input type="hidden" name="id" id="id">
                            <div class="row-form">
                                <label>Category Name</label>
                                <input type="text" name="category_name" id="category_name" placeholder="Product Category">
                                <small class="error_category_name"></small>
                            </div>
                            <button class="updateForm">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
                </div>
            </div>
        </div>
        <div class="modal bd-example-modal-lg" id="updateCategoryFormModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form">
                        <form id="updateCategoryProduct" method="post" autocomplete="off">
                            <input type="hidden" name="id" id="id">
                            <div class="row-form">
                                <label>Current Category Name</label>
                                <select  id="cat" name="cat" class="form-control">
                                    <option value="">-- Select Category --</option>
                                    @foreach ($category as $data)
                                    <option value="{{$data->cat_id}}">
                                        {{$data->category_name}}
                                    </option>
                                    @endforeach
                                </select>
                                <small class="error_cat"></small>
                                <label>New Category Name</label>
                                <input type="text" name="category_name" id="category_name" placeholder="Product Category">
                                <small class="error_category_name"></small>
                            </div>
                            <button class="updateForm">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
                </div>
            </div>
        </div>
        <div class="modal bd-example-modal-lg" id="deleteCategoryFormModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form">
                        <form id="deleteCategoryProduct" method="post" autocomplete="off">
                            <input type="hidden" name="id" id="id">
                            <div class="row-form">
                                <label>Category Name</label>
                                <select  id="cat" name="cat" class="form-control">
                                    <option value="">-- Select Category --</option>
                                    @foreach ($category as $data)
                                    <option value="{{$data->cat_id}}">
                                        {{$data->category_name}}
                                    </option>
                                    @endforeach
                                </select>
                                <small class="error_cat"></small>
                            </div>
                            <button class="deleteCategoryForm">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/form.js')}}"></script>
    <script src="{{ asset('js/datatable.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</html>
