<div class="sidebar" id="sidebar">
    <div id="head">
        <p class="logo">Auto GC</p>
        <img src="{{ asset('images/logo.jpg') }}" class="logo" alt="Logo">
    </div>
    <ul class="list">
        <li><a href="/dashboard">Dashboard</a></li>
        <li><a href="/admin-home">Add product</a></li>
		<li><a href="{{ url('/logout') }}"> logout </a></li>
    </ul>
</div>
<nav class="mobile-menu">
	<a class="logo" href="/">YourLogo</a>
	<a href="#" class="hamburger-wrapper">
		<div class="hamburger-menu"></div>
	</a>
	<div class="mobile-menu-overlay">
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Our mission</a></li>
			<li><a href="#">About us</a></li>
			<li><a href="#">Pricing</a></li>
			<li><a href="#">Products</a></li>
			<li><a href="#">Contact us</a></li>
		</ul>
	</div>
</nav>