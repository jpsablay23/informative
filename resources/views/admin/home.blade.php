<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Auto GC</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased">
        <div class="body-container">
            <div class="container">
                <div class="content">
                    <button onclick="document.getElementsByClassName('sidebar')[0].classList.toggle('collapsed')">
                        <div></div>
                        <div></div>
                        <div></div>
                    </button>
                </div>
                @include('admin.adminNav') 
            </div>
            <div class="content-form">
                <h4>Create Product</h4>
                <form id="product" method="post" autocomplete="off">
                    <div class="row-form">
                        <label>Product category</label>
                        <!-- <input type="text" name="category" id="category" placeholder="Product Category"> -->
                        <select  id="category" name="category" class="form-control">
                            <option value="">-- Select Category --</option>
                            @foreach ($category as $data)
                            <option value="{{$data->cat_id}}">
                                {{$data->category_name}}
                            </option>
                            @endforeach
                        </select>
                        <small class="error_category"></small>
                    </div>
                    <div class="row-form">
                        <label>Product name</label>
                        <input type="text" name="product_name" id="product_name" placeholder="Product Name">
                        <small class="error_product_name"></small>
                    </div>
                    <div class="row-form">
                        <label>Price</label>
                        <input type="number" name="price" id="price" placeholder="Price">
                        <small class="error_price"></small>
                    </div>
                    <div class="row-form">
                        <label>Quantity</label>
                        <input type="number" name="qty" id="qty" placeholder="quantity">
                        <small class="error_qty"></small>
                    </div>
                    <div class="row-form">
                        <label for="message">Product Description</label>
                        <textarea name="product_description" id="product_description" placeholder="Product description"></textarea>
                        <small class="error_product_description"></small>
                    </div>
                    <div class="row-form">
                        <label>Image</label>
                        <input type="file" name="image" id="image">
                        <small class="error_product_image"></small>
                    </div>
                    <button class="contact-us">Submit</button>
                </form>
            </div>
            <div class="modal fade" id="success" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        </div>
                        
                        <div class="modal-body">
                        
                            <div class="thank-you-pop">
                                <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                                <h1>Successfully added!</h1>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="{{ asset('js/form.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</html>
