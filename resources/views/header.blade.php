<div class="preloaderBg" id="preloader" onload="preloader()">
    <div class="preloader"></div>
    <div class="preloader2"></div>
</div>
<div class="header-container">
    <div class="header">
        <div class="logo">
            <img src="{{ asset('images/logo.jpg') }}" class="logo" alt="Logo">
        </div>
        <div class="menu">
            <ul>
                <li><a href="/">Home</a><li>
                <li><a class="services">Services</a><li>
                <li><a class="contact">Contact Us</a><li>
                <li><a class="partners">Partners</a><li>
                <li><a href="/products">Products</a><li>
            </ul>
        </div>
    </div>
</div>
<nav>
    <div class="navbar">
    <div class="container nav-container">
        <input class="checkbox" type="checkbox" name="" id="" />
        <div class="hamburger-lines">
            <span class="line line1"></span>
            <span class="line line2"></span>
            <span class="line line3"></span>
        </div>  
        <div class="hamburger-logo">
        <img src="{{ asset('images/logo.jpg') }}" class="h-logo" alt="Logo">
        </div>
        <div class="menu-items">
            @if (request()->is('products'))
                <li><a href="/">Home</a><li>
                <li><a href="/">Services</a><li>
                <li><a href="/">Contact Us</a><li>
                <li><a href="/">Partners</a><li>   
                <li><a href="/products">Products</a><li>
            @else
                <li><a href="/">Home</a><li>
                <li><a class="services">Services</a><li>
                <li><a class="contact">Contact Us</a><li>
                <li><a class="partners">Partners</a><li>
                <li><a href="/products">Products</a><li>
            @endif
        </div>
    </div>
    </div>
</nav>