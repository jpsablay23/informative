<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Auto GC</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.4/simplePagination.css" rel="stylesheet">
        <link href="{{ asset('css/product.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pop.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased">
        @include('header') 
        <div class="body-content">
            <div class="content-header">
                <h2>Products</h2>
            </div>
            <div class="content">
                <div class="left-content">
                    <h4>Categories</h4>
                    <ul>
                        @foreach ($categories as $category)
                        <li><a href="#" class="cat" value="{{ $category->cat_id }}">{{ $category->category_name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="right-content">
                <div class="content-container-flex">
                        @foreach ($products as $product)
                        <div class="products">
                            <div class="product-content">
                                <img src="{{ asset('images') }}/{{$product->image}}" class="product" alt="product">
                                <span class="prod-name">{{ $product->product_name }}</span>
                                <div class="product-desc">
                                {!! $product->product_description !!}
                                <span>₱ {{ $product->price }}</span>
                                </div>
                            </div>
                            <button class="product-btn" value="{{$product->id}}">Inquire</button>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div id="pagination-container">
            <ul id="pagin">
            </ul>
        </div>
        <div class="black-container">
            <div class="footer">
                <div class="left-footer">
                    <img src="{{ asset('images/logo.jpg') }}" class="logo" alt="Logo">
                    <div><label>Company Name © 2015</label></div>
                </div>
                <div class="right-footer">
                    <div class="company">
                    <p class="footer-company-about">
                        <span>About the company</span>
                        Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
                    </p>
                    </div>
                    <div class="social-content">
                        <ul>
                            <li><a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="img-popup">
            <img src="" alt="Popup Image">
        <div class="close-btn">
            <div class="bar"></div>
            <div class="bar"></div>
        </div>
        </div>
        <div class="modal bd-example-modal-lg" id="inquire" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inquire</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form">
                    <form id="inquire" method="post" autocomplete="off">
                        <input type="hidden" name="product_id" id="product_id" placeholder="Your Name">
                        <label for="name">Your Name</label>
                        <input type="text" name="name" id="name" placeholder="Your Name">
                        <small class="error_name"></small>
                        <label for="email">Email</label>
                        <input type="email" name="email"  id="email"placeholder="Email">
                        <small class="error_email"></small>
                        <label for="phone_number">Phone number</label>
                        <input type="number" name="phone_number"  id="phone_number"placeholder="Phone number">
                        <small class="error_phone_number"></small>
                        <label for="message">Message</label>
                        <textarea name="message" id="message" placeholder="Message"></textarea>
                        <small class="error_message"></small>
                        <button class="contact-us">Submit</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
        </div>
        <div class="modal fade" id="success" role="dialog" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Successfully added!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="inquiry_success" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Inquiry Sent!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </body>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.4/jquery.simplePagination.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</html>
