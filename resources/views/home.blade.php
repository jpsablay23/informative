<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Auto GC</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    </head>
    <body class="antialiased">
        @include('header') 
        <div class="banner">
        <img src="{{ asset('images/banner.jpg') }}" class="banner" alt="banner">
        </div>
        <div class="black-container">
            <div class="header-content">
                <h1>Our Services</h1>
            </div>
            <div class="">
                <div class="top-content">
                    <div class="content-description">
                        <h3>ROLLER LID COVER</h3>
                        <p>Roller lid covers are typically installed on the cargo bed of the vehicle using mounting brackets or clamps. Installation may require drilling holes in the bed rails or using existing attachment points, depending on the model and vehicle type. 300kgs capacity of roller lid cover which made of hard aluminum. They feature a retractable design that allows them to be easily opened or closed using a rolling mechanism. Some models may also come with locking mechanisms for added security. Roller lid covers are a popular accessory for pickup trucks and similar vehicles, offering security, protection, and convenience for the cargo area. They come in a variety of designs and styles to suit different preferences and needs.</p>
                        <a href="/products" class="more">more</a>
                    </div>
                    <div class="content-image">
                        <img src="{{ asset('images/rollerlid.png') }}" class="c-image">
                    </div>
                </div>
                <div class="top-content">
                    <div class="content-image">
                        <img src="{{ asset('images/rollbars.png') }}" class="c-image">
                    </div>
                    <div class="content-description">
                        <h3>ROLL BAR</h3>
                        <p>The primary purpose of roll bars is to enhance the structural integrity of a vehicle's chassis and provide protection for occupants in case of a rollover accident. They help prevent the roof from collapsing and provide a protective cage around the passenger compartment, reducing the risk of injury or fatality. Roll bars are typically made of high-strength steel or other durable materials capable of withstanding significant impact forces. They are strategically positioned and securely bolted to the vehicle's frame or chassis to ensure stability and strength.</p>
                        <a href="/products" class="more">more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-form">
            <div class="header-contact">
                <h1>Contact Us</h1>
            </div>
            <div class="contact-content">
                <div class="social">
                    <div class="details-content">
                        <ul>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i><label>Dasmariñas Cavite</label></li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i><label></label>09959019885</li>
                            <li><i class="fa fa-envelope" aria-hidden="true"></i><label></label>autogc.ph@gmail.com</li>
                        </ul>
                    </div>
                    <div class="social-content">
                        <ul>
                            <li><a href="https://www.facebook.com/profile.php?id=100066461363302"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="form">
                    <form id="inquire" method="post" autocomplete="off">
                        <label for="name">Your Name</label>
                        <input type="text" name="name" id="name" placeholder="Your Name">
                        <small class="error_name"></small>
                        <label for="email">Email</label>
                        <input type="email" name="email"  id="email"placeholder="Email">
                        <small class="error_email"></small>
                        <label for="phone_number">Phone number</label>
                        <input type="number" name="phone_number"  id="phone_number"placeholder="Phone number">
                        <small class="error_phone_number"></small>
                        <label for="message">Message</label>
                        <textarea name="message" id="message" placeholder="Message"></textarea>
                        <small class="error_message"></small>
                        <button class="contact-us">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="success" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Successfully added!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="inquiry_success" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="thank-you-pop">
                            <img class="svg" src = "{{ asset('images/check-circle.svg') }}" alt="My Happy SVG"/>
                            <h1>Inquiry Sent!</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="slider">
            <h1>Partners</h1>
            <div class="owl-carousel">
                <div class="slider-card">
                    <div class="d-flex justify-content-center align-items-center mb-4">
                        <img src="{{ asset('images/partner2.jpg') }}" class="c-image">
                    </div>
                </div>
                <div class="slider-card">
                    <div class="d-flex justify-content-center align-items-center mb-4">
                        <img src="{{ asset('images/partner3.jpg') }}" class="c-image">
                    </div>
                </div>
                <div class="slider-card">
                    <div class="d-flex justify-content-center align-items-center mb-4">
                        <img src="{{ asset('images/partner1.jpg') }}" class="c-image">
                    </div>
                </div>
            </div>
        </div>
        <div class="black-container footer-fixed">
            <div class="footer">
                <div class="left-footer">
                    <img src="{{ asset('images/logo.jpg') }}" class="logo" alt="Logo">
                    <div><label>Auto GC © 2020</label></div>
                </div>
                <div class="right-footer">
                    <div class="company">
                    <p class="footer-company-about">
                        <span>About the company</span>
                        <span class="company-desc">The Auto GC Car Accessories known to its dedication to quality, innovation and customer service. Highly experienced professionals offering quality car accessories services with its fully equipped latest automotive technologies. Offering variety of car accessories that include roller lid cover, roll bars, door visors and more by ensuring quality and efficiency in the products offered to the clients.</span>
                    </p>
                    </div>
                    <div class="social-content">
                        <ul>
                            <li><a href="https://www.facebook.com/profile.php?id=100066461363302"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/form.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</html>
