<!DOCTYPE html>
<html>
<head>
    <title>Thank You for Inquiring</title>
</head>

<body>
    <p>Hello {{ $testMailData['name'] }},</p>
    <p>{{ $testMailData['body'] }}</p>
    <p>Regards,<br>AutoGC PH</p>
    <img src="{{ asset('images/logo.jpg') }}" style="width:150px;" class="logo" alt="Logo">
</body>
</html>