<!DOCTYPE html>
<html>
<head>
    <title>Inquiry Details</title>
</head>
<body>
    
    <p >Hello AutoGC,</p>

    <p>here's the details of the inquirer</p>
    <p>Name: {{ $testMailData['name'] }}</p>
    <p>Product: 
    @if(isset($testMailData['product']))
        {{ $testMailData['product'] }}
    @endif    
    </p>
    <p>Phone number: {{ $testMailData['phone_number'] }}</p>
    <p>Email: {{ $testMailData['email'] }}</p>
    <p>Message: {{ $testMailData['message'] }}</p>
    <br>
    <p>Regards,<br>AutoGC PH</p>
    <img src="{{ asset('images/logo.jpg') }}" style="width:150px;" class="logo" alt="Logo">
</body>
</html>