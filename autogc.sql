-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2024 at 07:19 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autogc`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `category_name`, `updated_at`, `created_at`) VALUES
(1, 'Roller Lid V1 (Automatic)', '2024-01-31 18:59:28', '2024-01-21 22:31:32'),
(2, 'Roller Lid V2', '2024-01-31 18:59:01', '2024-01-23 18:05:08'),
(3, 'Roller Lid V3', '2024-01-31 18:58:26', '2024-01-31 17:53:08'),
(4, 'Rollbar', '2024-01-31 18:59:41', '2024-01-31 17:53:44'),
(5, 'Railguard', '2024-01-31 19:04:40', '2024-01-31 19:01:59'),
(6, 'Dashcam', '2024-01-31 19:06:02', '2024-01-31 19:06:02'),
(7, 'Window Visor', '2024-01-31 19:06:14', '2024-01-31 19:06:14'),
(8, 'Garnish (Black)', '2024-01-31 19:07:45', '2024-01-31 19:07:45'),
(9, 'Deep Dish Matting', '2024-01-31 19:07:54', '2024-01-31 19:07:54'),
(10, 'Step Sill', '2024-01-31 19:08:00', '2024-01-31 19:08:00'),
(11, 'Tailgate Assist', '2024-01-31 19:08:08', '2024-01-31 19:08:08'),
(12, 'Hood Damper', '2024-01-31 19:08:41', '2024-01-31 19:08:41'),
(13, 'Fender Slim', '2024-01-31 19:08:50', '2024-01-31 19:08:50'),
(14, 'Door side cladding', '2024-01-31 19:08:59', '2024-01-31 19:08:59'),
(15, 'Tailgate Cladding', '2024-01-31 19:09:07', '2024-01-31 19:09:07'),
(16, 'Roof Rack', '2024-01-31 19:09:59', '2024-01-31 19:09:59'),
(17, 'Awning', '2024-01-31 19:10:04', '2024-01-31 19:10:04'),
(18, 'Skid Plate', '2024-01-31 19:10:11', '2024-01-31 19:10:11'),
(19, 'Mud Guard', '2024-01-31 19:10:18', '2024-01-31 19:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inquire`
--

CREATE TABLE `inquire` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `product` varchar(255) DEFAULT NULL,
  `message` longtext NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inquire`
--

INSERT INTO `inquire` (`id`, `name`, `email`, `product`, `message`, `updated_at`, `created_at`) VALUES
(1, 'test', 'test@gmail.com', NULL, 'test', '2024-01-24 21:25:34', '2024-01-24 21:25:34'),
(2, 'test', 'test@gmail.com', NULL, 'test', '2024-01-24 21:26:38', '2024-01-24 21:26:38'),
(3, 'test', 'test@gmail.com', NULL, 'test', '2024-01-24 21:28:52', '2024-01-24 21:28:52'),
(4, 'test', 'test@gmail.com', NULL, 'test', '2024-01-24 21:32:36', '2024-01-24 21:32:36'),
(5, 'test', 'jpsablay23@gmail.com', NULL, 'test', '2024-01-24 21:47:11', '2024-01-24 21:47:11'),
(6, 'test', 'jpsablay23@gmail.com', NULL, 'test', '2024-01-24 22:15:54', '2024-01-24 22:15:54'),
(7, 'test', 'jhafsablay@gmail.com', NULL, 'test', '2024-01-24 22:19:21', '2024-01-24 22:19:21'),
(8, 'jhaf', 'jhafsablay@gmail.com', NULL, 'test', '2024-01-24 22:21:09', '2024-01-24 22:21:09'),
(9, 'test', 'jhafsablay@gmail.com', NULL, 'test', '2024-01-28 18:03:41', '2024-01-28 18:03:41'),
(10, 'test', 'jhafsablay@gmail.com', NULL, 'test', '2024-01-28 18:04:44', '2024-01-28 18:04:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2024_01_08_081249_product_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category`, `product_name`, `price`, `qty`, `product_description`, `image`, `created_at`, `updated_at`) VALUES
(1, '1', 'test', '33', '2', '<div>🔥SUPRA ROLLER LID🔥</div><div>🔥Stingray Rollbar🔥</div><div>💯Raptor 2022</div><div>✔️ 100% Waterproof/Leakproof</div><div>✔️ High Quality Hard Aluminum</div><div>✔️ Durable</div><div>✔️ Plug and Play</div><div>✔️ No Drilling Required</div><div>✔️ Smooth Opening System</div><div>✔️ Handle Lock with 2 Keys&nbsp;</div>', '1704702789.jpg', '2024-01-08 00:33:09', '2024-01-23 19:35:19'),
(2, '1', 'test 2', '222', '22', '<div>🔥SUPRA ROLLER LID🔥</div><div>🔥Stingray Rollbar🔥</div><div>💯Raptor 2022</div><div>✔️ 100% Waterproof/Leakproof</div><div>✔️ High Quality Hard Aluminum</div><div>✔️ Durable</div><div>✔️ Plug and Play</div><div>✔️ No Drilling Required</div><div>✔️ Smooth Opening System</div><div>✔️ Handle Lock with 2 Keys&nbsp;</div>', '1704784110.jpg', '2024-01-08 23:08:30', '2024-01-23 19:35:44'),
(3, '2', 'test3', '12', '2', '<div>🔥SUPRA ROLLER LID🔥</div><div>🔥Stingray Rollbar🔥</div><div>💯Raptor 2022</div><div>✔️ 100% Waterproof/Leakproof</div><div>✔️ High Quality Hard Aluminum</div><div>✔️ Durable</div><div>✔️ Plug and Play</div><div>✔️ No Drilling Required</div><div>✔️ Smooth Opening System</div><div>✔️ Handle Lock with 2 Keys&nbsp;</div>', '1704784264.jpg', '2024-01-08 23:11:04', '2024-01-23 19:35:25'),
(4, '1', 'test 4', '22', '2', '<div>🔥SUPRA ROLLER LID🔥</div><div>🔥Stingray Rollbar🔥</div><div>💯Raptor 2022</div><div>✔️ 100% Waterproof/Leakproof</div><div>✔️ High Quality Hard Aluminum</div><div>✔️ Durable</div><div>✔️ Plug and Play</div><div>✔️ No Drilling Required</div><div>✔️ Smooth Opening System</div><div>✔️ Handle Lock with 2 Keys&nbsp;</div>', '1704784485.jpg', '2024-01-08 23:14:45', '2024-01-23 19:35:50'),
(5, '2', 'test 5', '333', '3', '<div>🔥SUPRA ROLLER LID🔥</div><div>🔥Stingray Rollbar🔥</div><div>💯Raptor 2022</div><div>✔️ 100% Waterproof/Leakproof</div><div>✔️ High Quality Hard Aluminum</div><div>✔️ Durable</div><div>✔️ Plug and Play</div><div>✔️ No Drilling Required</div><div>✔️ Smooth Opening System</div><div>✔️ Handle Lock with 2 Keys&nbsp;</div>', '1704785452.jpg', '2024-01-08 23:30:52', '2024-01-23 19:35:35'),
(6, '11', 'tail gate', '22', '22', '<div>🔥SUPRA ROLLER LID🔥</div><div>🔥Stingray Rollbar🔥</div><div>💯Raptor 2022</div><div>✔️ 100% Waterproof/Leakproof</div><div>✔️ High Quality Hard Aluminum</div><div>✔️ Durable</div><div>✔️ Plug and Play</div><div>✔️ No Drilling Required</div><div>✔️ Smooth Opening System</div><div>✔️ Handle Lock with 2 Keys&nbsp;</div>', '1706757258.jpg', '2024-01-31 19:14:18', '2024-01-31 19:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jhaf', 'jhafsablay@gmail.com', NULL, '$2y$12$L24vze7VkthFZNMOEk0b.uQ6nJ7LD5zp7xviBKMdQ01ih.J5zDLgC', NULL, '2024-01-07 20:10:47', '2024-01-07 20:10:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `inquire`
--
ALTER TABLE `inquire`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inquire`
--
ALTER TABLE `inquire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
