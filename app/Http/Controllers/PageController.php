<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Validator;
use App\Models\Product;
use App\Models\Category;
use App\Models\Inquire;
use Mail;
use App\Mail\SendMail;
use App\Mail\InquireMail;

class PageController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function productPage($cat_id = null)
    {
      $category = Category::get()->first()->toArray();

      if(!empty($cat_id)){
          $cat_id = $cat_id;
      }else{
          $cat_id = $category['cat_id'];
      }

      $products['categories'] = Category::get();
      $products['products'] = Product::where('category','=',$cat_id)->get();
      return view('products',$products);
    }
    public function sendInquire(Request $request)
    {
      
      $validator = Validator::make($request->all(), [ 
        'name' => 'required', 
        'email' => 'required', 
        'phone_number' => 'required|numeric|digits:11',
        'message' => 'required'
      ]);
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors(), 'status'=>'failed']);

      }

      $postArray = $request->all(); 
      $items = Inquire::create($postArray); 

      if(!empty($request->product_id)){
        $product = Product::get()->where('id',$request->product_id)->first();
        $InquireMail = [
            'title' => 'Inquire Mail',
            'name' => $request->name,
            'product' => $product['product_name'],
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'message' => $request->message
        ];
      }else{
        $InquireMail = [
            'title' => 'AutoGC PH auto-email',
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'message' => $request->message
        ];
      }

      
      $defaultmMail = [
        'name' => $request->name,
        'title' => 'Inquire Mail',
        'body' => 'Thank you for inquiring, will get back to you within 1-2 business days'
      ];

      Mail::to('georgedaulong@gmail.com')->send(new SendMail($InquireMail));
      Mail::to($request->email)->send(new InquireMail($defaultmMail));
      
      // send output data to vue3
      return response()->json([
        'status' => 'success',
      ]); 
    }
}
