<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Validator;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function loginHome()
    {
        return view('auth.login');
    }
    public function adminHome()
    {
        $products['category'] = Category::get();
        return view('admin.home',$products);
    }
  
    public function productList()
    {
      $products['data'] = Product::leftJoin('category', 'category.cat_id', '=', 'product.category')->get()->toArray();
      return $products;
    }
    public function getProduct(Request $request)
    {
      $products['data'] = Product::get()->where('id',$request->id)->first();
      return $products;
    }
    public function dashboard()
    {
      $products['category'] = Category::get();
      return view('admin.dashboard',$products);
    }
    public function saveProduct(Request $request)
    {
    //data validation
      $validator = Validator::make($request->all(), [ 
        'category' => 'required', 
        'product_name' => 'required', 
        'price' => 'required', 
        'qty' => 'required', 
        'product_description' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
      ]);
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors(), 'status'=>'failed']);

      }
      if ($request->has('image') || $request->image != 'null'){
        $imageName = time().'.'.$request->image->extension();  
      
        $request->image->move(public_path('images'), $imageName);
      }

      $postArray = $request->all(); 
      $postArray['image'] = $imageName;
      $items = Product::create($postArray); 
      
      // send output data to vue3
      return response()->json([
        'status' => 'success',
      ]); 
                
        return view('admin.home');
    }
    public function saveCategory(Request $request)
    {
    //data validation
      $validator = Validator::make($request->all(), [ 
        'category_name' => 'required', 
      ]);
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors(), 'status'=>'failed']);

      }
      $postArray['category_name'] = $request->category_name;
      $items = Category::create($postArray); 
      
      // send output data to vue3
      return response()->json([
        'status' => 'success',
      ]); 
                
        return view('admin.dashboard');
    }
    public function updateCategory(Request $request)
    {
    //data validation
      $validator = Validator::make($request->all(), [ 
        'cat' => 'required', 
        'category_name' => 'required', 
      ]);
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors(), 'status'=>'failed']);
      }

      Category::where('cat_id', '=', $request->cat)
        ->update([
          'category_name' =>$request->category_name,
        ]);
      
      // send output data to vue3
      return response()->json([
        'status' => 'success',
      ]); 
                
        return view('admin.dashboard');
    }
    public function updateProduct(Request $request)
    {
    //data validation
      $validator = Validator::make($request->all(), [ 
        'category' => 'required', 
        'product_name' => 'required', 
        'price' => 'required', 
        'qty' => 'required', 
        'product_description' => 'required',
        'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
      ]);
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors(), 'status'=>'failed']);

      }
      if ($request->image != ''){
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('images'), $imageName);

        Product::where('id', '=', $request->id)
        ->update([
          'category' => $request->category, 
          'product_name' => $request->product_name, 
          'price' => $request->price, 
          'qty' => $request->qty, 
          'product_description' => $request->product_description, 
          'image' => $imageName
        ]);
      }else{
        Product::where('id', '=', $request->id)
        ->update([
          'category' => $request->category, 
          'product_name' => $request->product_name, 
          'price' => $request->price, 
          'qty' => $request->qty, 
          'product_description' => $request->product_description
        ]);
      }

      
      
      // send output data to vue3
      return response()->json([
        'status' => 'success',
      ]); 
                
        return view('admin.home');
    }
    public function deleteProduct($id)
    {
      Product::where('id',$id)->delete();
      return response()->json([
        'status' => 'success',
      ]); 
                
        return view('admin.home');
    }
    public function deleteCategory(Request $request)
    {
      Category::where('cat_id', '=', $request->cat)->delete();
      return response()->json([
        'status' => 'success',
      ]); 
                
      return view('admin.home');
    }
}
