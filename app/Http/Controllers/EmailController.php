<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMail;
use App\Mail\InquireMail;

class EmailController extends Controller
{
    public function index()
    {
        $testMailData = [
            'title' => 'Test Email From AllPHPTricks.com',
            'body' => 'This is the body of test email.'
        ];

        Mail::to('jhafsablay@gmail.com')->send(new SendMail($testMailData));

        dd('Success! Email has been sent successfully.');
    }
    public function inquire()
    {
        $testMailData = [
            'title' => 'Inquire Mail',
            'body' => 'This is the body of test email.'
        ];

        Mail::to('jhafsablay@gmail.com')->send(new InquireMail($testMailData));

        dd('Success! Email has been sent successfully.');
    }
}
