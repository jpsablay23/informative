<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquire extends Model
{
    use HasFactory;

    protected $table = 'inquire';

    protected $fillable = [
        'name',
        'email',
        'product',
        'message'
    ];
}
