$(document).ready(function() {
    $('.createCategory').on('click', function(e) {
        $("#categoryFormModal").modal('show');
    });
    $('.updateCategory').on('click', function(e) {
        $("#updateCategoryFormModal").modal('show');
    });
    $('.deleteCategory').on('click', function(e) {
        $("#deleteCategoryFormModal").modal('show');
    });
    $('#products').DataTable( {
        ajax: '/productList',
        columns: [
            { data: 'category_name' },
            { data: 'product_name' },
            { data: 'price' },
            { data: 'qty' },
            { data: 'product_description' },
            { data: 'image' ,
                "render": function (data) {
                    return '<img src="' + 'images/' + data + '" class="avatar" width="150" height="150"/>';
                }
            },
            { "data": 'id',
            "render": function (data) {
               return '<button class="edit-btn btn-primary" value="'+data+'">Edit</button>\
               <button class="delete-product btn-warning" value="'+data+'">Delete</button>';
            }
        },
        ],
        "initComplete": function(){
            $('.delete-product').on('click', function(e) {
                $('.delete').val($(this).val());
                $("#deleteModal").modal('show');
            });
            $('.edit-btn').on('click', function(e) {
                var id = $(this).val();
                $("#updateFormModal").modal('show');
                $.get('getProduct', { id: id}, 
                    function(product){
                        $("#category").val(product.data.category).change();
                        // $('#category').val(product.data.category);
                        $('#product_name').val(product.data.product_name);
                        $('#price').val(product.data.price);
                        $('#qty').val(product.data.qty);
                        $('#id').val(product.data.id);
                        if(product.data.product_description == ''){
                            $('#product_description').summernote('code', '');
                        }
                        $('#product_description').summernote('code', product.data.product_description); 
                        if(product.data.image != ''){
                            $('.check_image').removeClass('hide');
                            $('.change_image').removeClass('hide');
                            $(".check_image img").attr("src", "images/"+ product.data.image +"");
                            $('.has_image').addClass('hide');
                        }else{
                            
                        }
                });
            });
            $('.delete').on('click', function(e) {
                var id = $(this).val();
                console.log(id);
                $.ajax({
                    url: '/dashboard/delete_product/'+id,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    id: id,
                    success: function (data) {
                        
                        if(data.status == 'success'){
                            $("#deleteModal").modal('hide');
                            $("#success-delete").modal('show');
                            $('.thank-you-pop .close').on('click', function(e) {
                                $("#success-delete").modal('hide');
                                location.reload();
                            });
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
            $('.close').on('click', function(e) {
                $("#updateFormModal").modal('hide');
                $("#categoryFormModal").modal('hide');
                $("#updateCategoryFormModal").modal('hide');
                $("#deleteCategoryFormModal").modal('hide');
                $("#deleteModal").modal('hide');
            });
            $('.change_image').on('click', function(e) {
                $('.check_image').addClass('hide');
                $('.change_image').addClass('hide');
                $('.has_image').removeClass('hide');
            });
          }
    } );
    $("form#updateProduct").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);

        $.ajax({
            url: '/dashboard/update_product',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function (data) {
                
                if(data.status == 'success'){
                    $("#updateFormModal").modal('hide');
                    $("#success").modal('show');
                    $('.thank-you-pop .close').on('click', function(e) {
                        $("#success").modal('hide');
                        location.reload();
                    });
                }else if(data.status == 'failed'){
                    clearValidationAddForm();
                  $.each(data.error,function(key, val){
                        $("small.error_" + key).text(val[0]);
                    });
                  }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $("#categoryProduct").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);

        $.ajax({
            url: '/dashboard/save_category',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function (data) {
                
                if(data.status == 'success'){
                    $("#categoryFormModal").modal('hide');
                    $("#success").modal('show');
                    $('.thank-you-pop .close').on('click', function(e) {
                        $("#success").modal('hide');
                        location.reload();
                    });
                }else if(data.status == 'failed'){
                    clearValidationAddForm();
                  $.each(data.error,function(key, val){
                        $("small.error_" + key).text(val[0]);
                    });
                  }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $("#updateCategoryProduct").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);

        $.ajax({
            url: '/dashboard/update_category',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function (data) {
                
                if(data.status == 'success'){
                    $("#updateCategoryFormModal").modal('hide');
                    $("#updateModal").modal('show');
                    $('.thank-you-pop .close').on('click', function(e) {
                        $("#success").modal('hide');
                        location.reload();
                    });
                }else if(data.status == 'failed'){
                    clearValidationAddForm();
                  $.each(data.error,function(key, val){
                        $("small.error_" + key).text(val[0]);
                    });
                  }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $("#deleteCategoryProduct").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);
       
        $.ajax({
            url: '/dashboard/delete_category',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function (data) {
                
                if(data.status == 'success'){
                    $("#deleteCategoryFormModal").modal('hide');
                    $("#success-delete").modal('show');
                    $('.thank-you-pop .close').on('click', function(e) {
                        $("#success-delete").modal('hide');
                        location.reload();
                    });
                }else if(data.status == 'failed'){
                    clearValidationAddForm();
                  $.each(data.error,function(key, val){
                        $("small.error_" + key).text(val[0]);
                    });
                  }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    function clearValidationAddForm(){
		$('small.error_category').text('');
		$('small.error_product_name').text('');
		$('small.error_price').text('');
        $('small.error_qty').text('');
        $('small.error_product_description').text('');
        $('small.error_image').text('');
	}
    $('#product_description').summernote();
});