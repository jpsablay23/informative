$(document).ready(function() {
    $("form#product").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);

        $.ajax({
            url: '/admin-home/save_product',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function (data) {
                
                if(data.status == 'success'){
                    $("#success").modal('show');
                }else if(data.status == 'failed'){
                    clearValidationAddForm();
                  $.each(data.error,function(key, val){
                        $("small.error_" + key).text(val[0]);
                    });
                  }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $('.close').on('click', function(e) {
        $("#success").modal('hide');
        $("#inquiry_success").modal('hide');
        $("#product").trigger('reset');
    });
    function clearValidationAddForm(){
		$('small.error_category').text('');
		$('small.error_product_name').text('');
		$('small.error_price').text('');
        $('small.error_qty').text('');
        $('small.error_product_description').text('');
        $('small.error_image').text('');
	}

    $("form#inquire").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);
        $('.contact-us').prop('disabled', true);
        $(".contact-us").css( "background", "gray"); 
        $.ajax({
            url: '/inquire',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function (data) {
                
                if(data.status == 'success'){
                    $("#inquiry_success").modal('show');
                    $('.contact-us').prop('disabled', false);
                    $(".contact-us").css( "background", "gray"); 
                }else if(data.status == 'failed'){
                    clearValidationInquireForm();
                  $.each(data.error,function(key, val){
                        $("small.error_" + key).text(val[0]);
                    });
                    $('.contact-us').prop('disabled', false);
                    $(".contact-us").css( "background", "black"); 
                  }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $('#success .close').on('click', function(e) {
        $("#success").modal('hide');
        location.reload();
    });
    $('#inquiry_success .close').on('click', function(e) {
        $("#inquiry_success").modal('hide');
        location.reload();
    });
    function clearValidationInquireForm(){
		$('small.error_name').text('');
		$('small.error_email').text('');
		$('small.error_message').text('');
        $('small.error_phone_number').text('');
	}
    $(".services").click(function() {
        $('html, body').animate({
            scrollTop: $(".black-container").offset().top
        }, 200);
    });
    $(".contact").click(function() {
      $('html, body').animate({
          scrollTop: $(".contact-form").offset().top
      }, 200);
  });
  $(".partners").click(function() {
    $('html, body').animate({
        scrollTop: $(".slider").offset().top
    }, 200);
});
  $('#preloader').delay(2000).hide(0); 
  $('.menu-items a').click(function() {
    $('.checkbox').trigger('click');
 });
    // Owlcarousel
    $(".owl-carousel").owlCarousel({
      responsive:{
            0:{
                items:1,
                nav:true,
                loop:true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
            },
            600:{
                items:2,
                nav:false,
                loop:true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
            },
            1100:{
                items:3,
                nav:false,
                loop:true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
            }
        }
    });
    $(window).resize(function() {
        if ( $(window).width() < 854 ) {
          startCarousel();
        } else {
          stopCarousel();
        }
    });
});