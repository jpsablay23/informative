$(document).ready(function() {

    // required elements
    var imgPopup = $('.img-popup');
    var imgCont  = $('.product-content');
    var popupImage = $('.img-popup img');
    var closeBtn = $('.close-btn');
    
    // handle events
    imgCont.on('click', function() {
      var img_src = $(this).children('img').attr('src');
      imgPopup.children('img').attr('src', img_src);
      imgPopup.addClass('opened');
    });
  
    $(imgPopup, closeBtn).on('click', function() {
      imgPopup.removeClass('opened');
      imgPopup.children('img').attr('src', '');
    });
  
    popupImage.on('click', function(e) {
      e.stopPropagation();
    });
    
    $('.product-btn').on('click', function(e) {
        var id = $(this).val();
        $('#product_id').val(id);
        $('#inquire').show();
        $('#inquire').addClass('overlay-bg');
    });
    $('.close').on('click', function(e) {
        $('.modal').hide();
        $('.modal').removeClass('overlay-bg');
    });

    (function($) {
      var pagify = {
        items: {},
        container: null,
        totalPages: 1,
        perPage: 2,
        currentPage: 0,
        createNavigation: function() {
          this.totalPages = Math.ceil(this.items.length / this.perPage);
    
          $('.pagination', this.container.parent()).remove();
          var pagination = $('<div class="pagination"></div>').append('<a class="nav prev disabled" data-next="false"><</a>');
    
          for (var i = 0; i < this.totalPages; i++) {
            var pageElClass = "page";
            if (!i)
              pageElClass = "page current";
            var pageEl = '<a class="' + pageElClass + '" data-page="' + (
            i + 1) + '">' + (
            i + 1) + "</a>";
            pagination.append(pageEl);
          }
          pagination.append('<a class="nav next" data-next="true">></a>');
    
          this.container.after(pagination);
    
          var that = this;
          $("body").off("click", ".nav");
          this.navigator = $("body").on("click", ".nav", function() {
            var el = $(this);
            that.navigate(el.data("next"));
          });
    
          $("body").off("click", ".page");
          this.pageNavigator = $("body").on("click", ".page", function() {
            var el = $(this);
            that.goToPage(el.data("page"));
          });
        },
        navigate: function(next) {
          // default perPage to 5
          if (isNaN(next) || next === undefined) {
            next = true;
          }
          $(".pagination .nav").removeClass("disabled");
          if (next) {
            this.currentPage++;
            if (this.currentPage > (this.totalPages - 1))
              this.currentPage = (this.totalPages - 1);
            if (this.currentPage == (this.totalPages - 1))
              $(".pagination .nav.next").addClass("disabled");
            }
          else {
            this.currentPage--;
            if (this.currentPage < 0)
              this.currentPage = 0;
            if (this.currentPage == 0)
              $(".pagination .nav.prev").addClass("disabled");
            }
    
          this.showItems();
        },
        updateNavigation: function() {
    
          var pages = $(".pagination .page");
          pages.removeClass("current");
          $('.pagination .page[data-page="' + (
          this.currentPage + 1) + '"]').addClass("current");
        },
        goToPage: function(page) {
    
          this.currentPage = page - 1;
    
          $(".pagination .nav").removeClass("disabled");
          if (this.currentPage == (this.totalPages - 1))
            $(".pagination .nav.next").addClass("disabled");
    
          if (this.currentPage == 0)
            $(".pagination .nav.prev").addClass("disabled");
          this.showItems();
        },
        showItems: function() {
          this.items.hide();
          var base = this.perPage * this.currentPage;
          this.items.slice(base, base + this.perPage).show();
    
          this.updateNavigation();
        },
        init: function(container, items, perPage) {
          this.container = container;
          this.currentPage = 0;
          this.totalPages = 1;
          this.perPage = perPage;
          this.items = items;
          this.createNavigation();
          this.showItems();
        }
      };
    
      // stuff it all into a jQuery method!
      $.fn.pagify = function(perPage, itemSelector) {
        var el = $(this);
        var items = $(itemSelector, el);
    
        // default perPage to 5
        if (isNaN(perPage) || perPage === undefined) {
          perPage = 3;
        }
    
        // don't fire if fewer items than perPage
        if (items.length <= perPage) {
          return true;
        }
    
        pagify.init(el, items, perPage);
      };
    })(jQuery);
    
    $(".content-container-flex").pagify(3, ".products");
    
    $('.cat').click(function(e) {
      e.preventDefault();
      window.location.href = "/products/"+ $(this).attr('value');
  });

  $("form#inquire").submit(function(e) {
      e.preventDefault();    
      var formData = new FormData(this);
      $('.contact-us').prop('disabled', true);
      $(".contact-us").css( "background", "gray"); 
      $.ajax({
          url: '/products/inquire',
          type: 'POST',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: formData,
          success: function (data) {
              
              if(data.status == 'success'){
                  $("#inquiry_success").modal('show');
                  $('.contact-us').prop('disabled', false);
                  $(".contact-us").css( "background", "gray"); 
              }else if(data.status == 'failed'){
                clearValidationInquireForm();
                $.each(data.error,function(key, val){
                      $("small.error_" + key).text(val[0]);
                  });
                  $('.contact-us').prop('disabled', false);
                  $(".contact-us").css( "background", "black"); 
                }
          },
          cache: false,
          contentType: false,
          processData: false
      });
  });
  $('#success .close').on('click', function(e) {
      $("#success").modal('hide');
      location.reload();
  });
  $('#inquiry_success .close').on('click', function(e) {
    $("#inquiry_success").modal('hide');
    location.reload();
  });
  function clearValidationInquireForm(){
    $('small.error_name').text('');
    $('small.error_email').text('');
    $('small.error_message').text('');
    $('small.error_phone_number').text('');
  }
  $('#preloader').delay(2000).hide(0); 
  });