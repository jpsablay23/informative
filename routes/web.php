<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Mail\Markdown;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


 Route::get('login',array('as'=>'login',function(){
    return view('auth.login');
}));
Route::get('/products', 'App\Http\Controllers\PageController@productPage');
Route::get('/products/{cat_id}', 'App\Http\Controllers\PageController@productPage');
Route::get('/', 'App\Http\Controllers\PageController@home');
Route::post('/inquire','App\Http\Controllers\PageController@sendInquire');
Route::post('/products/inquire','App\Http\Controllers\PageController@sendInquire');
Route::get('/send-email', 'App\Http\Controllers\EmailController@index');
Route::get('/send-email-inquire', 'App\Http\Controllers\EmailController@inquire');
Route::get('register',array('as'=>'register',function(){
    return view('auth.register');
}));
Route::get('/test-email', function() {
    $testMailData = [
        'name' => 'Test',
        'title' => 'Test Email',
        'body' => 'This is the body of test email.'
    ];
    return new App\Mail\InquireMail($testMailData);
 });
Auth::routes();

Route::group(['middleware' => ['auth']], function () { 
    Route::post('/admin-home/save_product','App\Http\Controllers\HomeController@saveProduct');
    Route::post('/dashboard/update_product','App\Http\Controllers\HomeController@updateProduct');
    Route::delete('/dashboard/delete_product/{id}','App\Http\Controllers\HomeController@deleteProduct');
    Route::post('/dashboard/save_category','App\Http\Controllers\HomeController@saveCategory');
    Route::post('/dashboard/delete_category','App\Http\Controllers\HomeController@deleteCategory');
    Route::post('/dashboard/update_category','App\Http\Controllers\HomeController@updateCategory');
    
   
    Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout');
    // Route::get('admin-home',array('as'=>'admin-home',function(){
    //     return view('admin.home');
    // }));
    // Route::get('dashboard',array('as'=>'dashboard',function(){
    //     return view('admin.dashboard');
    // }));
    Route::get('/dashboard', 'App\Http\Controllers\HomeController@dashboard');
    Route::get('/admin-home', 'App\Http\Controllers\HomeController@adminHome');
    Route::get('/productList', 'App\Http\Controllers\HomeController@productList');
    Route::get('/getProduct', 'App\Http\Controllers\HomeController@getProduct');
});

